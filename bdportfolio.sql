CREATE DATABASE IF NOT EXISTS PORTFOLIO;
USE PORTFOLIO;
CREATE TABLE IF NOT EXISTS LenguajesProgramacion (
    id INT PRIMARY KEY,
    nombre VARCHAR(50),
    creador VARCHAR(50),
    año_lanzamiento INT,
    nivel VARCHAR(50)
);

INSERT INTO LenguajesProgramacion (id, nombre, creador, año_lanzamiento, ncontactomodelidemailcontactomodelproyectosProyectosproyectosProyectosherramientasmodelherramientasmodelcontactomodeldireccioncontactomodelContactoemailivel) VALUES
(1, 'Python', 'Guido van Rossum', 1991, 'Medio'),
(2, 'Java', 'James Gosling', 1995, 'Avanzado'),
(3, 'JavaScript', 'Brendan Eich', 1995, 'Medio'),
(4, 'C++', 'Bjarne Stroustrup', 1985, 'Avanzado');

CREATE TABLE IF NOT EXISTS HerramientasOffice (
    id INT PRIMARY KEY,
    nombre VARCHAR(50),
    desarrollador VARCHAR(50),
    año_lanzamiento INT,
    plataforma VARCHAR(50)
);

INSERT INTO HerramientasOffice (id, nombre, desarrollador, año_lanzamiento, plataforma) VALUES
(1, 'Microsoft Word', 'Microsoft', 1983, 'Windows, macOS'),
(2, 'Google Sheets', 'Google', 2006, 'Web'),
(3, 'LibreOffice Writer', 'The Document Foundation', 2011, 'Windows, macOS, Linux'),
(4, 'Apple Pages', 'Apple Inc.', 2005, 'macOS, iOS');

CREATE TABLE IF NOT EXISTS Contacto (
    id INT PRIMARY KEY,
    nombre VARCHAR(50),
    email VARCHAR(50),
    telefono VARCHAR(20),
    empresa VARCHAR(50),
    direccion VARCHAR(100)
);

INSERT INTO Contacto (id, nombre, email, telefono, empresa, direccion) VALUES
(1, 'Lucia Rojas', 'rojasdelgadolucia@gmail.com', '+34600000000', 'Cesur', 'Calle Ave del Paraiso 7');

CREATE TABLE IF NOT EXISTS Proyectos (
    id INT PRIMARY KEY,
    nombre VARCHAR(100),
    descripcion TEXT,
    fecha_inicio DATE,
    fecha_fin DATE,
    contacto_id INT,
    FOREIGN KEY (contacto_id) REFERENCES Contacto(id)
);

INSERT INTO Proyectos (id, nombre, descripcion, fecha_inicio, fecha_fin, contacto_id) VALUES
(1, 'Sistema de Gestión', 'Desarrollo de un sistema de gestión para la empresa A', '2023-01-01', '2023-06-30', 1),
(2, 'Página Web Corporativa', 'Diseño y desarrollo de la nueva página web para la empresa B', '2023-02-15', '2023-05-15', 1),
(3, 'Aplicación Móvil', 'Creación de una aplicación móvil para la empresa C', '2023-03-01', '2023-08-31', 1),
(4, 'Intranet Corporativa', 'Implementación de una intranet para la empresa D', '2023-04-01', '2023-09-30', 1);


