package com.example.springportfolio2.services;

import com.example.springportfolio2.model.lenguajesmodel;
import com.example.springportfolio2.repository.lenguajesrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class lenguajesservices {

    @Autowired
    private lenguajesrepository lenguajesProgramacionRepository;

    public List<lenguajesmodel> getAllLenguajesProgramacion() {
        return lenguajesProgramacionRepository.findAll();
    }
}
