package com.example.springportfolio2.services;

import com.example.springportfolio2.model.proyectosmodel;
import com.example.springportfolio2.repository.proyectosrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
public class proyectosservices {

    private final proyectosrepository proyectoRepository;

    @Autowired
    public proyectosservices(proyectosrepository proyectoRepository) {
        this.proyectoRepository = proyectoRepository;
    }
    public List<proyectosmodel> getAllProyectos() {
        return proyectoRepository.findAll();
    }
}

