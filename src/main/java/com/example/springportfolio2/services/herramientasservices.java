package com.example.springportfolio2.services;

import com.example.springportfolio2.model.herramientasmodel;
import com.example.springportfolio2.repository.herramientasrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class herramientasservices {

    @Autowired
    private herramientasrepository herramientasOfficeRepository;

    public List<herramientasmodel> getAllHerramientasOffice() {
        return herramientasOfficeRepository.findAll();
    }

}

