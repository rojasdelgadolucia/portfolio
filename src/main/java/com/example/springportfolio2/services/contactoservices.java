package com.example.springportfolio2.services;

import com.example.springportfolio2.model.contactomodel;
import com.example.springportfolio2.repository.contactorepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class contactoservices {

    @Autowired
    private contactorepository contactoRepository;

    public List<contactomodel> getAllContactos() {
        return contactoRepository.findAll();
    }
}
