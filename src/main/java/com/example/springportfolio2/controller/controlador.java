package com.example.springportfolio2.controller;

import com.example.springportfolio2.model.contactomodel;
import com.example.springportfolio2.model.herramientasmodel;
import com.example.springportfolio2.model.lenguajesmodel;
import com.example.springportfolio2.model.proyectosmodel;
import com.example.springportfolio2.services.contactoservices;
import com.example.springportfolio2.services.herramientasservices;
import com.example.springportfolio2.services.lenguajesservices;
import com.example.springportfolio2.services.proyectosservices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
public class controlador {

    @Autowired
    private contactoservices contactoService;

    @Autowired
    private proyectosservices proyectosService;

    @Autowired
    private lenguajesservices lenguajesService;

    @Autowired
    private herramientasservices herramientasService;

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("Contacto", contactoService.getAllContactos());
        model.addAttribute("lenguajes", lenguajesService.getAllLenguajesProgramacion());
        model.addAttribute("Proyectos", proyectosService.getAllProyectos());
        model.addAttribute("herramientas", herramientasService.getAllHerramientasOffice());
        return "index";
    }

    @RestController
    @RequestMapping("/api")
    public class ApiController {

        @GetMapping("/contactos")
        public List<contactomodel> getAllContactos() {
            return contactoService.getAllContactos();
        }

        @GetMapping("/herramientas")
        public List<herramientasmodel> getAllHerramientas() {
            return herramientasService.getAllHerramientasOffice();
        }

        @GetMapping("/lenguajes")
        public List<lenguajesmodel> getAllLenguajes() {
            return lenguajesService.getAllLenguajesProgramacion();
        }

        @GetMapping("/proyectos")
        public List<proyectosmodel> getAllProyectos() {
            return proyectosService.getAllProyectos();
        }
    }
}
