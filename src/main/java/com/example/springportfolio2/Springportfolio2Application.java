package com.example.springportfolio2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springportfolio2Application {

	public static void main(String[] args) {
		SpringApplication.run(Springportfolio2Application.class, args);
	}

}
