package com.example.springportfolio2.repository;

import com.example.springportfolio2.model.lenguajesmodel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface lenguajesrepository extends JpaRepository<lenguajesmodel, Integer> {
}
