package com.example.springportfolio2.repository;

import com.example.springportfolio2.model.herramientasmodel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface herramientasrepository extends JpaRepository<herramientasmodel, Integer> {
}
