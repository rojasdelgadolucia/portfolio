package com.example.springportfolio2.model;

import jakarta.persistence.*;

@Entity
@Table (name= "LenguajesProgramacion")
public class lenguajesmodel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private int id;
    private String nombre;
    private String creador;
    private int año_lanzamiento;
    private String nivel;

    // Getters y Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCreador() {
        return creador;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    public int getAñoLanzamiento() {
        return año_lanzamiento;
    }

    public void setAñoLanzamiento(int año_lanzamiento) {
        this.año_lanzamiento = año_lanzamiento;
    }

    public String getTipo() {
        return nivel;
    }

    public void setTipo(String tipo) {
        this.nivel = tipo;
    }
}
