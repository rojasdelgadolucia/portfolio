package com.example.springportfolio2.model;

import jakarta.persistence.*;

@Entity
@Table(name= "HerramientasOffice")
public class herramientasmodel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;
    private String desarrollador;
    private int año_lanzamiento;
    private String plataforma;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDesarrollador() {
        return desarrollador;
    }

    public void setDesarrollador(String desarrollador) {
        this.desarrollador = desarrollador;
    }

    public int getAñoLanzamiento() {
        return año_lanzamiento;
    }

    public void setAñoLanzamiento(int año_lanzamiento) {
        this.año_lanzamiento = año_lanzamiento;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }
}

